package tp.cozi;

import java.util.Iterator;
import java.util.List;

import tp.cozi.gui.SimulationFrame;
import tp.cozi.strategy.SelectionPolicy;

/**
 * Clasa cu ajutorul careia vom porni simularea.
 * 
 * @author olarp
 *
 */
public class App implements Runnable {
	// Variabile parametrii pentru simulare
	public int timeLimit = 100;
	public int maxNoServers = 3;
	public int maxNoCustomersPerServer = 20;
	public int maxProcessingTime = 10;
	public int minProcessingTime = 2;
	public int numberOfClients = 100;
	public SelectionPolicy selectionPolicy = SelectionPolicy.SHORTEST_TIME;

	private Scheduler scheduler;
	private SimulationFrame frame;
	private List<Customer> generatedCustomers;
	private CustomerGenerator generator;

	public App() {
		// Initializam scheduler cu numarul maxim de cozi si numarul maxim de clienti pe
		// cozi
		scheduler = new Scheduler(maxNoServers, maxNoCustomersPerServer);
		// Seteaza strategia de asezare a clientilor in coada
		scheduler.changeStrategy(selectionPolicy);
		// Cream si afisam fereastra GUI
		frame = new SimulationFrame();
		// Initializam obiectul cu care vom genera clienti
		generator = new CustomerGenerator(minProcessingTime, maxProcessingTime, numberOfClients);
		// Initializam lista de clienti generati
		generatedCustomers = generator.generateRandomCustomers();
	}

	/**
	 * Metoda care proceseaza simularea. Interam pana la timpul limita si parcurgem
	 * lista de clienti generati. Folosind scheduler o sa trimitem fiecare client in
	 * coada cea mai potrivita, updatam UI-ul odata la 500 de milisecunde.
	 */
	public void run() {
		int currentTime = 0;
		// Cat timp nu am ajuns la timpul limita.
		while (currentTime < timeLimit) {
			try {
				// Parcurgem lista de clienti generati cu iterator pentru ca vrem sa ii si
				// stergem din lista
				for (Iterator<Customer> iterator = generatedCustomers.iterator(); iterator.hasNext();) {
					// Luam clientul curent
					Customer customer = iterator.next();
					// Daca timpul de sosire al clientului curent este egal cu timpul simularii.
					if (customer.getArrivalTime() == currentTime) {
						// Trimitem clientul la coada cea mai potrivita
						scheduler.dispatchCustomer(customer);
						// Scoatem clientul din lista
						iterator.remove();
					}
				}
				// Updatam UI-ul
				frame.displayData(getLabels(), getCustomers());
				// Incrementam timpul curent
				currentTime++;
				Thread.sleep(500);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}

	}

	/**
	 * Metoda ce genereaza o matrice de clienti(avem nevoie pentru a afisa in
	 * JTable) parcurgand toate cozile din simulare.
	 * 
	 * @return
	 */

	private Customer[][] getCustomers() {
		//Luam numarul de cozi din simulare
		int noOfServers = scheduler.getServers().size();
		//Initalizam matricea
		Customer[][] customers = new Customer[maxNoCustomersPerServer][noOfServers];
		//Luam lista de cozi
		List<Server> servers = scheduler.getServers();
		//Parcurgem cozile
		for (int i = 0; i < servers.size(); i++) {
			//Luam coada curenta
			Server server = servers.get(i);
			//Luam vectorul de clienti din coada 
			Customer[] customersPerServer = server.getCustomers();
			//Parcurgem vectorul de clienti
			for (int j = 0; j < customersPerServer.length; j++) {
				//Adaugam in matrice clientul curent
				customers[j][i] = customersPerServer[j];
			}
		}
		return customers;
	}

	/**
	 * Returneaza un vector de stringuri ce reprezinta header-ul tabelului(coada 1,
	 * coada 2, coada 3)
	 * 
	 * @return
	 */
	private String[] getLabels() {
		String[] labels = new String[maxNoServers];
		for (int i = 0; i < maxNoServers; i++) {
			labels[i] = "Coada " + (i + 1);
		}
		return labels;
	}

	public static void main(String[] args) {
		App app = new App();
		Thread a = new Thread(app);
		a.start();
	}

}
