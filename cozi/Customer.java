package tp.cozi;

/**
 * Reprezinta clientul pe care il vom aseza la coada. Aceasta clasa
 * implementeaza interfata comparable pentru ca vom avea nevoie sa sortam lista
 * de clienti pe care ii vom aseza la coada. Suprascriind metoda compareTo
 * definim criteriul de comparare pentru clienti. Acesta este timpul la care au
 * ajuns in magazin (arrivalTime).
 * 
 * @author olarp
 *
 */
public class Customer implements Comparable<Customer> {
	// Timpul la care a ajuns in magazin
	private int arrivalTime;
	// Timpul de stat la coada(timpul de procesare).
	private int processingTime;

	public Customer(int arrivalTime, int processingTime) {
		this.arrivalTime = arrivalTime;
		this.processingTime = processingTime;
	}

	public int getArrivalTime() {
		return arrivalTime;
	}

	public void setArrivalTime(int arrivalTime) {
		this.arrivalTime = arrivalTime;
	}

	public int getProcessingTime() {
		return processingTime;
	}

	public void setProcessingTime(int processingTime) {
		this.processingTime = processingTime;
	}

	/**
	 * Returneaza reprezentarea string a obiectului customer, ea se constuieste
	 * afisand cuvantul customer si timpul la care a ajuns in magazin
	 */
	@Override
	public String toString() {
		return "Customer " + arrivalTime;
	}

	/**
	 * Metoda ce defineste criteriul de comparare al obiectelor de tip customer.
	 * Daca returneaza o valoare negativa inseamna ca timpul obiectului curent este
	 * mai mic decat timpul obiectului de comparat. Daca returneaza 0 inseamna ca
	 * clientii au ajuns in acelasi timp. Daca ii mai mare inseamna ca clientul cu
	 * care comparam a ajuns mai repede decat clientul curent.
	 */
	public int compareTo(Customer arg0) {
		return arrivalTime - arg0.arrivalTime;
	}

}
