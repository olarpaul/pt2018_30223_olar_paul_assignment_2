package tp.cozi;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;

/**
 * Aceasta clasa va genera o lista de clienti care se vor atribui in cozi.
 * Fiecarui client i se va genera timpul de procesare intre valoare
 * minpProcessTime si maxProcessTime. Aceasta lista va avea numberOfCustomers
 * elemente.
 * 
 * @author olarp
 *
 */
public class CustomerGenerator {
	// limita pentru timpul de sosire in magazin(arrivalTime)
	public static final int TIME_LIMIT = 100;
	// Obiect cu care vom genera valori intregi random
	private static final Random random = new Random();
	private int minProcessTime;
	private int maxProcessTime;
	private int numberOfCustomers;

	public CustomerGenerator(int minProcessTime, int maxProcessTime, int numberOfCustomers) {
		this.minProcessTime = minProcessTime;
		this.maxProcessTime = maxProcessTime;
		this.numberOfCustomers = numberOfCustomers;
	}

	/**
	 * Returneaza o lista de clienti de dimensiunea numberOfCustomers.
	 * @return
	 */
	public List<Customer> generateRandomCustomers() {
		//se intializeaza lista
		List<Customer> generatedCustomers = new ArrayList<Customer>();
		for (int i = 0; i < numberOfCustomers; i++) {
			//se adauga in lista un client generat cu generateCustomer
			generatedCustomers.add(generateCustomer());
		}
		//Sorteaza lista de clienti
		Collections.sort(generatedCustomers);
		return generatedCustomers;
	}

	/**
	 * Genereaza un client avand timpul de sosire in magazin maxim TIME_LIMIT si
	 * timpul de procesare cumprins intre minProcessTime si maxProcessTime
	 * 
	 * @return
	 */
	private Customer generateCustomer() {
		int arrivalTime = random.nextInt(TIME_LIMIT);
		int processingTime = random.nextInt(maxProcessTime - minProcessTime) + maxProcessTime;
		return new Customer(arrivalTime, processingTime);
	}
}
