package tp.cozi;

import java.util.ArrayList;
import java.util.List;

import tp.cozi.strategy.ConcreteStrategyQueue;
import tp.cozi.strategy.ConcreteStrategyTime;
import tp.cozi.strategy.SelectionPolicy;
import tp.cozi.strategy.Strategy;

/**
 * Aceasta clasa are responsablitatea de a distribui clienti in cozi(servere) in
 * functie de strategia aleasa.
 * 
 * @author olarp
 *
 */
public class Scheduler {
	// Lista de cozi in care se vor distribui clienti
	private List<Server> servers;
	// Numarul maxim de cozi
	private int maxNoServers;
	// Numarul maxim de clienti pe cozi
	private int maxCustomersPerServer;
	// Strategia dupa care se alege coada in care se va merge clientul
	private Strategy strategy;

	/**
	 * Initalizeaza valorile pt numarul maxim de cozi si numarul maxim de clienti pe
	 * coada. Initalizeaza si cozile
	 * 
	 * @param maxNoServers
	 * @param maxCustomersPerServer
	 */
	public Scheduler(int maxNoServers, int maxCustomersPerServer) {
		this.maxNoServers = maxNoServers;
		this.maxCustomersPerServer = maxCustomersPerServer;
		initServers();
	}

	/**
	 * Metoda de intializare a cozilor, va popula liste de cozi si va porni
	 * threadurile aferente fiecarei cozi
	 */
	private void initServers() {
		// initalizam lista de cozi
		this.servers = new ArrayList<Server>();
		// Intr-un for pana la numarul maxim de cozi
		for (int i = 0; i < maxNoServers; i++) {
			// Se creaza noua coada
			Server server = new Server(maxCustomersPerServer);
			// Se adauga la lista
			servers.add(server);
			// Se creaaza threadul avand ca si parametru coada proaspat creata
			Thread t = new Thread(server);
			// Apelam metoda start ca sa pornim thread-ul
			t.start();
		}
	}

	/**
	 * Metoda care seteaza strategia dupa care se vor insera clientii in coada. Se
	 * verifica policy-ul trimis ca si parametrul si se instantieaza strategy cu
	 * implementarea concreta (cea mai scurta coada sau cel mai scurt timp).
	 * 
	 * @param policy
	 */
	public void changeStrategy(SelectionPolicy policy) {
		if (policy == SelectionPolicy.SHORTEST_QUEUE) {
			strategy = new ConcreteStrategyQueue();
		} else if (policy == SelectionPolicy.SHORTEST_TIME) {
			strategy = new ConcreteStrategyTime();
		}
	}

	/**
	 * Metoda se va apela pentru a adauga un client in coada cea mai potrivita. Se
	 * apeleaza metoda addCustomer din strategy
	 * 
	 * @param customer
	 */
	public void dispatchCustomer(Customer customer) {
		strategy.addCustomer(servers, customer);
	}

	/**
	 * Returneaza o lista de cozi
	 * 
	 * @return
	 */
	public List<Server> getServers() {
		return servers;
	}

}
