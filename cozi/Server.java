package tp.cozi;

import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * Aceasta clasa reprezinta modelul unei cozi in care se vor adauga clienti.
 * Aceasta clasa implementeaza Runnable deci ii este asignat un thread in care
 * se vor procesa clientii.
 * 
 * @author olarp
 *
 */
public class Server implements Runnable {
	// Numarul maxim de clienti posibili pe o coada.
	private static final int MAX_CAPACITY = 100;
	// Coada de clienti
	private BlockingQueue<Customer> customerQueue;
	// Reprezinta timpul de asteptare pe coada
	private AtomicInteger waitingPeriod;

	/**
	 * Constructor implicit. Apelam celalalt constructor cu capacitate maxima
	 */
	public Server() {
		this(MAX_CAPACITY);
	}

	/**
	 * Constructor customizat, ia ca si parametru capacitatea maxima a cozii. In el
	 * intializam waiting period cu 0 si coada de clienti cu capacitatea data ca si
	 * parametru
	 * 
	 * @param maxCapacity
	 */
	public Server(int maxCapacity) {
		waitingPeriod = new AtomicInteger();
		customerQueue = new ArrayBlockingQueue<Customer>(maxCapacity);
	}

	/**
	 * Adauga un client in coada. Se incrementeaza timpul de asteptare la coada cu
	 * timpul de procesare al clientului de adaugat
	 * 
	 * @param newCustomer
	 */
	public void addCustomer(Customer newCustomer) {
		customerQueue.add(newCustomer);
		waitingPeriod.addAndGet(newCustomer.getProcessingTime());
	}

	/**
	 * Aceasta metoda se va apela cand se porneste thread-ul ce proceseaza aceasta
	 * coada(cand se apeleaza Thread.start()). Metoda care proceseaza clientii in
	 * coada folosind while(true) pentru ca vrem sa executam codul dinauntru atata
	 * timp cat thread-ul este pornit.
	 */
	public void run() {
		while (true) {
			try {
				// Se ia clientul de prima pozitie din coada
				Customer customerToTake = customerQueue.take();
				// Opeste executia pentru timpul de procesare al clientului. Inmultim cu 200 de
				// milisecunde aceasta valoare pentru ca simularea sa fie mai vizibila
				Thread.sleep(customerToTake.getProcessingTime() * 200);
				// Decrementam timpul de asteptare pe coada cu timpul clientului care tocmai a
				// fost servit
				waitingPeriod.set(waitingPeriod.get() - customerToTake.getProcessingTime());
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}

	/**
	 * Aceasta metoda returneaza sub forma de vector clientii din coada
	 * 
	 * @return
	 */
	public Customer[] getCustomers() {
		return customerQueue.toArray(new Customer[customerQueue.size()]);
	}

	/**
	 * Aceasta metoda returneaza timpul de asteptare pe coada curenta
	 * 
	 * @return
	 */
	public int getWaitingPeriod() {
		return waitingPeriod.get();
	}

}
