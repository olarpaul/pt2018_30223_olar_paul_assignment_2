package tp.cozi.gui;

import java.awt.Dimension;
import java.awt.GridLayout;

import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;

/**
 * Reprezinta un panel in care vom aseza clienti la coada, fiecare coloana
 * reprezinta o coada si fiecare rand este cate un client asezat la coada
 * aferenta.
 * 
 * @author olarp
 *
 */
public class QueuesTablePanel extends JPanel {

	private static final long serialVersionUID = 1L;

	public QueuesTablePanel(String[] columnNames, Object[][] data) {
		super(new GridLayout(1, 0));

		final JTable table = new JTable(data, columnNames);
		//Dimensiunea tabelului
		table.setPreferredScrollableViewportSize(new Dimension(500, 250));
		table.setFillsViewportHeight(true);

		// Creem scrollPane
		JScrollPane scrollPane = new JScrollPane(table);

		// Il adaugam la panel
		add(scrollPane);
	}
}