package tp.cozi.gui;

import javax.swing.JFrame;

import tp.cozi.Customer;

/**
 * Reprezinta fereastra GUI. Este un JFrame in care vom adauga un JTable
 * reprezentand pe fiecare coloana cozile in care clientii vor sta la coada
 * 
 * @author olarp
 *
 */
public class SimulationFrame extends JFrame {
	private static final long serialVersionUID = 1L;

	public SimulationFrame() {
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		// Pentru ca fereastra sa se deschida in mijlocului ecranului si nu in coltul
		// din stanga sus
		setLocationRelativeTo(null);
		setVisible(true);
	}

	/**
	 * Metoda ce updateaza tabelul in functie de schimbarile ce au loc in cozi, se
	 * adauga clienti la coada si clienti sunt serviti(ies din coada)
	 * @param labels
	 * @param customers
	 */
	public void displayData(String[] labels, Customer[][] customers) {
		QueuesTablePanel tabelPanel = new QueuesTablePanel(labels, customers);
		tabelPanel.setOpaque(true);
		setContentPane(tabelPanel);
		pack();
	}

}
