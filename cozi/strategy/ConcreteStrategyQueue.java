package tp.cozi.strategy;

import java.util.List;

import tp.cozi.Customer;
import tp.cozi.Server;

/**
 * Reprezinta implementarea concreta a strategiei de coada cu coada cea mai
 * scurta. Aceasta va adauga clientul in coada care are cel mai mic numar de
 * clienti in ea.
 * 
 * @author olarp
 *
 */
public class ConcreteStrategyQueue implements Strategy {

	public void addCustomer(List<Server> servers, Customer customer) {
		int minQueueSize = getMinQueueSize(servers);
		for (Server server : servers) {
			if (minQueueSize == server.getCustomers().length) {
				server.addCustomer(customer);
				return;
			}
		}
	}

	private int getMinQueueSize(List<Server> servers) {
		int minQueueSize = Integer.MAX_VALUE;
		for (Server server : servers) {
			if (minQueueSize > server.getCustomers().length) {
				minQueueSize = server.getCustomers().length;
			}
		}
		return minQueueSize;
	}
}
