package tp.cozi.strategy;

import java.util.List;

import tp.cozi.Customer;
import tp.cozi.Server;

/**
 * Reprezinta implementarea concreta a strategiei de coada cu timpul cel mai
 * scurt. Aceasta va adauga clientul in coada care are timpul de asteptare cel
 * mai mic
 * 
 * @author olarp
 *
 */
public class ConcreteStrategyTime implements Strategy {
	/**
	 * Implementarea metodei din interfata strategy
	 */
	public void addCustomer(List<Server> servers, Customer customer) {
		// Luam timpul de asteptare minim de pe toate cozile
		int minTimeInQueue = getMinInTimeQueue(servers);
		// Parcurgem lista de cozi
		for (Server server : servers) {
			// Verificam timpul minim de asteptare cu timpul cozii curente, daca sunt
			// egale adaugam clientul in coada curenta
			if (minTimeInQueue == server.getWaitingPeriod()) {
				server.addCustomer(customer);
				// Am gasit coada cea mai potrivita si iesim din for
				return;
			}
		}
	}

	/**
	 * Metoda ce parcurge toate cozile si returneaza timpul minim de asteptare de pe
	 * toate cozile.
	 * 
	 * @param servers
	 * @return
	 */
	private int getMinInTimeQueue(List<Server> servers) {
		// Initializam cu o valoarea foarte mare(maximul de la intreg)
		int minTimeInQueue = Integer.MAX_VALUE;
		for (Server server : servers) {
			if (server.getWaitingPeriod() < minTimeInQueue) {
				minTimeInQueue = server.getWaitingPeriod();
			}
		}
		return minTimeInQueue;
	}
}
