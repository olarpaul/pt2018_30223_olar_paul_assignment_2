package tp.cozi.strategy;

/**
 * Reprezinta cele doua criterii dupa care vom adauga clientii la coada,
 * respectiv cea mai scurta coada sau cel mai scurt timp pe coada.
 * 
 * @author olarp
 *
 */
public enum SelectionPolicy {
	SHORTEST_QUEUE, SHORTEST_TIME;
}
