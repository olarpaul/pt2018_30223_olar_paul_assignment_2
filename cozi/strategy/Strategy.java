package tp.cozi.strategy;

import java.util.List;

import tp.cozi.Customer;
import tp.cozi.Server;

/**
 * Interfata ce isi defineste metoda de adaugat clientul la coada cea mai
 * potrivita, ia ca si parametru lista de cozi disponibile si clientul de
 * adaugat
 * 
 * @author olarp
 *
 */
public interface Strategy {
	public void addCustomer(List<Server> servers, Customer customer);
}
